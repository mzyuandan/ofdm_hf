function output=delete_cp(input,cp_length,sub_num_cp,sub_length_cp)

input_tem = reshape(input,sub_num_cp,sub_length_cp);
output=zeros(sub_num_cp-cp_length,sub_length_cp);
for j = 1:sub_length_cp%这里的数据长度是没有变的，是加入导频后的信号长度
    output(1:(sub_num_cp-cp_length),j)=input_tem((cp_length+1):sub_num_cp,j);
end
end