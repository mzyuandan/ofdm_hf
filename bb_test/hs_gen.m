clc;
clear all;
close all;

NB = 32;
KB = 16;
MB = 16;
Z =	256;
Z_MAX = 256;

file_name = '1d2_8192.txt';
save_name = 'sh_1d2_16X32_256.mat';

[hl_dec, hl_enc]= Get_SH(MB,NB,Z,Z_MAX, file_name);
% [hl_dec, hl_enc]= Get_SH_mod(MB,NB,Z, file_name);


save(save_name, 'hl_dec', 'hl_enc');
aa = 0;
