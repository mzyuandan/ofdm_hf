function [output,count,pilot_seq]=insert_pilot(input,pilot_bit,pilot_inter,pilot_typ)%pilot_inter������һ��������ݶ�
[dataout_I,dataout_Q]=modulation(pilot_bit);
pilot_symbol= dataout_I+1i*dataout_Q;
[N,NL]=size(input);
count =0;
i=1;
switch pilot_typ 
    case 1%%�����
        pilot_seq =reshape(pilot_symbol,length(pilot_bit)/2,1);
        output=zeros(N,(NL+fix(NL/pilot_inter)));
        while i<(NL+fix(NL/pilot_inter))
            output(:,i)=pilot_seq;
            count=count+1;
            if count*pilot_inter<=NL
                output(:,(i+1):(i+pilot_inter))=input(:,((count-1)*pilot_inter+1):count*pilot_inter);
            else
                output(:,(i+1):(i+pilot_inter+NL-count*pilot_inter))=input(:,((count-1)*pilot_inter+1):NL);
            end
            i=i+pilot_inter+1;
        end
        output = [output pilot_seq];
    case 2%%�����
        pilot_seq =pilot_symbol;
        output=zeros(N+fix(N/pilot_inter),NL);
        while(i<N+fix(N/pilot_inter))
            output(i,:)=pilot_seq;
            count=count+1;
            if count*pilot_inter<=N
                output((i+1):(i+pilot_inter),:)=input((count-1)*pilot_inter+1:count*pilot_inter,:);
            else
                output((i+1):(i+pilot_inter+N-count*pilot_inter),:)=input(((count-1)*pilot_inter+1:N),:);
            end
            i=i+pilot_inter+1;
        end
            
            
end
