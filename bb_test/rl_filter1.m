function [data_rec,data_rec_e, h] = rl_filter1( data_trans,ch,snr,flag,MM,mp)
len = length(data_trans);

Ts = ch.InputSamplePeriod;
path = round(ch.PathDelays ./ Ts);
pn = length(path);

if flag==0
        data_rec_e= filter(ch, data_trans);
         path_gain = ch.PathGains;
    %     根据带宽进行修改
        h = zeros(len,MM);
        for ii=1:1:pn
            h(:,path(ii)+mp) = path_gain(:,ii);
        end
else
    data_rec_e = data_trans;
    h = zeros(len,MM);
    h(:,mp) = ones(length(data_trans),1);
end 
data_rec = awgn (data_rec_e, snr);
end