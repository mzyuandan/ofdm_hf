function output=Insert_CP(input,cp_length)%这一步在加入导频后
[sub_num,sub_length]=size(input);
output=zeros(sub_num+cp_length,sub_length);%每个子载波上一个点，所有子载波相同位置组合就是一个完整符号
for j=1:sub_length
    output(1:cp_length,j)=input((sub_num-cp_length+1):sub_num,j);
   output((cp_length+1):(sub_num+cp_length),j)=input(:,j);
end