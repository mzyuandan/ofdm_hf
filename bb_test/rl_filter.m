function [ data_rec, data_rec_e, h ] = rl_filter( data_trans, ch, fd, snr,flag, M, mp)
len = length(data_trans);

Ts = ch.InputSamplePeriod;
path = round(ch.PathDelays ./ Ts);
pn = length(path);


if flag==0
    if fd==0
        pg = ones(1, pn);
        pg(1) = 0.1;
        data_rec_e = pg(1) * data_trans;
        for ii=2:1:pn
            pg(ii) = exp(1i*2*pi*rand(1,1));
            data_rec_e = data_rec_e + [zeros(1, path(ii)), data_trans(1:len-path(ii)) * pg(ii)];
        end
        data_rec_e = data_rec_e * (1/sqrt(pn));
        h = zeros(len,M);
        for ii=1:1:pn
            h(:,path(ii)+mp) = 1/sqrt(pn) * pg(ii);
        end
    else
        data_rec_e = filter(ch, data_trans);
        path_gain = ch.PathGains;
    %     根据带宽进行修改
        h = zeros(len,M);
        for ii=1:1:pn
            h(:,path(ii)+mp) = path_gain(:,ii);
        end
    end
else
    data_rec_e = data_trans;
    h = zeros(len,M);
    h(:,mp)=1;
end 
data_rec = awgn (data_rec_e, snr);
end