function freq_veiw( data, fs )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    len = length(data);

    fdata = fft(data);
    fdata = [fdata(len/2+1:len) fdata(1:len/2)];

    fi1 = (0:1:(len/2-1)) * (fs/len);
    fi2 = (-len/2:1:-1) * (fs/len);
    fi = [fi2 fi1];


    figure;
    plot(fi, 20*log10(abs(fdata)));


end

