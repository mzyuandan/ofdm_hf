function [dataout_I,dataout_Q] = demodulation(data_in)
dataout_I=(1-sign(real(data_in)))/2;
dataout_Q=(1-sign(imag(data_in)))/2;
end
