function[output]=ls_estimation(input,H_ifft,pilot_seq,count,pilot_inter,pilot_typ, N0,w)
[sub_num,sub_length]=size(input);
output=zeros(sub_num,sub_length);
[row,col]=size(H_ifft);
H=fft(H_ifft,row)/sqrt(row);
input_tem=fft(input,sub_num)/sqrt(sub_num);
switch pilot_typ
    case 1
        for ii=1:1:count+1
            H_out(:,ii) = H(:,ii)./(pilot_seq);
        end
        for ii=1:1:count
            for jj=1:1:pilot_inter
                Hi = H_out(:,ii)*w(jj,1) + H_out(:,ii+1)*w(jj,2);
                eh = abs(Hi).^2;
                output(:,(ii-1)*pilot_inter+jj) = input_tem(:,(ii-1)*pilot_inter+jj) .* conj(Hi(:,1)) ./ (eh+N0);
            end
        end
    case 2
        for i=1:1:count
            H_out(i,:) = H(i,:)./(pilot_seq);
         end
        k=1;
        q=1;
        b=1;
        while q<=count
            h=inv(diag(H_out(b,:),0));
            if(k+pilot_inter-1>sub_num)
                Y=input_tem(k:sub_num,:);
                output(k:sub_num,:)=Y*h;
            else
                Y=input_tem(k:(k+pilot_inter-1),:);
                output(k:(k+pilot_inter-1),:)=Y*h;
            end
            k=k+pilot_inter;
            q=q+1;
            b=b+1;
        end
end