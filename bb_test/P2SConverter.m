function data_out=P2SConverter(data_inI,data_inQ)
%函数功能为：将相干接收后的并行信号转化为串行信号
%输入：data_inI,data_inQ为输入的IQ两路信号
%输出：data_out，为串行输出bits
L=length(data_inI);
data_out=zeros(1,2*L);
        for k=1:L
%             data_out(2*k-1:2*k)=[data_inI(k) data_inQ(k)];
              data_out(1,2*k-1) = data_inI(k);
              data_out(1,2*k) = data_inQ(k);
        end
    
end