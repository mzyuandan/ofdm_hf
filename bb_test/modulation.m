function [dataout_I,dataout_Q] = modulation(data_in)
NOR=1/sqrt(2);
L=fix(length(data_in)/2);
dataout_I=zeros(1,L);
dataout_Q=zeros(1,L);

for ii = 1:L
    dataout_I(ii)=NOR*(1-2*data_in(2*ii-1));
    dataout_Q(ii)=NOR*(1-2*data_in(2*ii));
end
