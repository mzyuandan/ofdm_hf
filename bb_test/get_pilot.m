function[output,H]=get_pilot(input,pilot_inter,count,pilot_typ)%inputȥ����CP
[sub_num,sub_length]=size(input);
switch pilot_typ
    case 1
        output_tem = zeros(sub_num,sub_length-count-1);
        H_tem =zeros(sub_num,count+1);
        k=1;
        i=1;
        while(k<=count+1)
            H_tem(:,k)=input(:,(pilot_inter+1)*i-pilot_inter);
            k=k+1;
            i=i+1;
        end
        j=2;
        q=1;
        r=1;
        while(q<=sub_length-count-1)
            output_tem(:,q:(q+(pilot_inter-1)))=input(:,j:(j+pilot_inter-1));
            q=pilot_inter*r+1;
            j=(pilot_inter+1)*r+2;
            r=r+1;
        end
        output=output_tem;
        H=H_tem;
    case 2
        output_tem=zeros(sub_num-count,sub_length);
        H_tem =zeros(count,sub_length);
        k=1;
        i=1;
        while(k<=count)
            H_tem(k,:)=input((pilot_inter+1)*i-pilot_inter,:);
            k=k+1;
            i=i+1;

        end
        j=2;
        q=1;
        r=1;
        while(q<=sub_num-count)
            if q+(pilot_inter-1)>sub_num-count
                output_tem(q:(sub_num-count),:)=input(j:sub_num,:);
            else
                output_tem(q:(q+(pilot_inter-1)),:)=input(j:(j+pilot_inter-1),:);
            end
            q=pilot_inter*r+1;
            j=(pilot_inter+1)*r+2;
            r=r+1;
        end
        output=output_tem;
        H=H_tem;
end