clear;
close all;
warning off;
clc;

load 'sh_1d2_16X32_256.mat';
z = 256;
nb = 32;
mb = 16;
kb = 16;
code_rate = 1/2;
cod_len = nb * z;
bit_len = cod_len * code_rate;

%ǰ�����?
fs = 48e3;
num_carriers = 2048;
f_delta = fs/(num_carriers);
cp_length=120;
pilot_interval=1;
% pilot_interval_fre=1;       %test for 2, 3
M = 2; 
MM=cp_length;
mp=1;
cod_num = 20;
% w = [2/3, 1/3; 1/3, 2/3;];
w = [1/2, 1/2];

symbol_len = cod_num*cod_len / (num_carriers*M);
pilot_typ=1;%��Ƶ���뷽ʽ1.��2.��3.��
if pilot_typ==1
    pilot_bit_1 = source(2*num_carriers);
elseif pilot_typ==2
    pilot_bit_1 = source(2*symbol_len);
elseif pilot_typ==3
    %��״����
end
h_fig = 0;
err_allow =100;
OFDM_SNR_BER= zeros(1,31);
OFDM_LS_SNR_BER= zeros(1,31); 
 
snr_cnt=1;
chanflag = 0;
Channelestimationmethod = 1;


bit_tx = source(bit_len*cod_num); 
cod_tx_p = [];
for ii=1:1:cod_num
    cod_tp = step(hl_enc, bit_tx((ii-1)*bit_len+1:(ii)*bit_len).');
    cod_tp = cod_tp.';
    cod_tx_p = [cod_tx_p; cod_tp];
end
cod_tx = reshape(cod_tx_p.', 1, cod_len*cod_num);
iter_tx = reshape(cod_tx_p, 1, cod_len*cod_num);


[moddata_outI,moddata_outQ]=modulation(iter_tx);
OFDMmoddata_in_temp = moddata_outI+1i*moddata_outQ;
OFDMmoddata_in = reshape(OFDMmoddata_in_temp,num_carriers,(length(OFDMmoddata_in_temp))/num_carriers);
[Insertpilot_out,count,pilot_seq] = insert_pilot(OFDMmoddata_in,pilot_bit_1,pilot_interval,pilot_typ);

fft_n = num_carriers;

OFDMmoddata_out = ifft(Insertpilot_out,fft_n)*sqrt(fft_n);
InsertCPdata_out=Insert_CP(OFDMmoddata_out,cp_length);
if pilot_typ==1
    symbol_new=count+symbol_len+1;
    num_carriers_new=num_carriers+cp_length;
elseif pilot_typ==2
    symbol_new=symbol_len;
    num_carriers_new=num_carriers+cp_length+count;
end
data_trans = reshape(InsertCPdata_out,1,symbol_new*num_carriers_new);
tao=0.002;
fd=1;
%fs=f_delta*num_carriers;
delay = [0 tao];
pd = [0 0];
ch = rayleighchan(1/fs, fd,delay, pd);
ch.dopplerspectrum = doppler.gaussian;
ch.storepathgains =1;
if (fd~=0)
    ch.storehistory = 1;
end
ch.resetbeforefiltering =0;

for snr=10:1:30
    eb_n0 = 10 ^ (snr/10.0);
    n0 = 1/((eb_n0)+1);
    sigma = n0 /2;
    
    n_iter = 0;         % 累加译码迭代次数
    m_iter = 0;         % 累加均衡迭代次数
    
    frm_num = 0;        % 数据帧数
    gogo = 1;           % 仿真继续标志
    
    de_be = 0;          % 累加可检测错误比特数
    un_be = 0;          % 累加不可检测错误比特数
    de_fe = 0;          % 累加可检测错误帧数
    un_fe = 0;          % 累加不可检测错误帧数
    de_be_e = 0;        % 记录累加可检测错误比特数
    un_be_e = 0;        % 记录累加不可检测错误比特数
    de_fe_e = 0;        % 记录累加可检测错误帧数
    un_fe_e = 0;        % 记录累加不可检测错误帧数
    bit_err = 0;        % 一帧内的错误比特数
    biterr_e=0;         % 记录一帧内的错误比特数
    be2 = 0;
    
    while (gogo==1)
    
        [data_rec,data_rec_e, h]= rl_filter(data_trans,ch,fd, snr,chanflag,MM,mp);%ȥ��CP���Ѿ����˴���ת����

        hx = h(cp_length+num_carriers/2,:);
        for ii=1:1:(pilot_interval+1)*symbol_len/pilot_interval-1
            hx = [hx;  h(cp_length+num_carriers/2+ii*(num_carriers+cp_length),:);];
        end
        hxx = [hx zeros((pilot_interval+1)*symbol_len/pilot_interval, num_carriers-cp_length)];
        hxx = hxx.';
        hf = fft(hxx, num_carriers);
        if (h_fig==1)
            figure; plot(abs(hx(1,:)));
            figure; plot(real(hf(:,1)));
            figure; plot(real(hf(1,:)));
        end

        data_rec_delCP=delete_cp(data_rec,cp_length,num_carriers_new,symbol_new);

        [data_rec_delCP_delpilot,H]=get_pilot(data_rec_delCP,pilot_interval,count,pilot_typ);
    %     data_rec_delCP_delpilot = data_rec_delCP;

        %figure;plot(abs(H(:,1)), 'r');hold on; plot(abs(H(:,2)), 'k');
        OFDM_Demodulationdata_out= fft(data_rec_delCP_delpilot)/sqrt(fft_n); 
        if (h_fig==1)
            eh = abs(hf(:,1)).^2;
            equ_dat = 1*OFDM_Demodulationdata_out(:,1) .* conj(hf(:,1)) ./ eh;
            figure; plot(real(Insertpilot_out(:,1))); hold on; plot(real(equ_dat(:,1)), 'r');

            rxd = Insertpilot_out(:,1) .* hf(:,1);
            figure; plot(real(OFDM_Demodulationdata_out(:,1))); hold on; plot(real(rxd(:,1)), 'r');

            he_q = OFDM_Demodulationdata_out(:,1) ./ Insertpilot_out(:,1);
            he_h = OFDM_Demodulationdata_out(:,4) ./ Insertpilot_out(:,4);
            he = he_q*(2/3)+he_h*(1/3);
            he2 = OFDM_Demodulationdata_out(:,2) ./ Insertpilot_out(:,2);
            figure; plot(real(he2(:,1))); hold on; plot(real(he(:,1)), 'r');

            ehe = abs(he(:,1)).^2;
            equ_dat_e = 1*OFDM_Demodulationdata_out(:,2) .* conj(he(:,1)) ./ ehe;
            figure; plot(real(Insertpilot_out(:,2))); hold on; plot(real(equ_dat_e(:,1)), 'r');

        end

        OFDM_Demodulationdata_out_1=reshape(OFDM_Demodulationdata_out,1,num_carriers*symbol_len);
        [demodulationdata_outI_1,demodulationdata_outQ_1]=demodulation(OFDM_Demodulationdata_out_1);
        data_out_1=P2SConverter(demodulationdata_outI_1,demodulationdata_outQ_1);
        [~,ber1]= symerr(cod_tx, data_out_1);             %OFDM��һ��ֱ�ӵ���õ���������?
        OFDM_SNR_BER(snr_cnt) = ber1; 


        % 进行均衡
        OFDM_Demodulationdata_out_ls=ls_estimation(data_rec_delCP_delpilot,H,pilot_seq,count,pilot_interval,pilot_typ, 0, w);%fft���ڲ�������
        OFDM_Demodulationdata_out_2=reshape(OFDM_Demodulationdata_out_ls,1,num_carriers*symbol_len);
        llr_p = [2*real(OFDM_Demodulationdata_out_2)/n0; 2*imag(OFDM_Demodulationdata_out_2)/n0;];
        llr = reshape(llr_p, 1, cod_len*cod_num);
        for ii=1:1:cod_len*cod_num
            if(llr(ii)>25)
                llr(ii) = 25;
            end
            if(llr(ii)<-25)
                llr(ii) = -25;
            end
        end
        
        llr_tp = reshape(llr, cod_num, cod_len);
        llr_inter = reshape(llr_tp.', 1, cod_len*cod_num);
        

        for ii=1:1:cod_num
            [de_be, un_be, de_fe, un_fe, n_iter, dblk, llr_dp, c_flag(ii), be_now] = decode_mat(hl_dec, ...
                llr_inter((ii-1)*cod_len+1:(ii)*cod_len), cod_tx((ii-1)*cod_len+1:(ii)*cod_len), mb, nb, z, de_be, un_be, de_fe, un_fe, n_iter, 0);
        end

        [demodulationdata_outI_2,demodulationdata_outQ_2]=demodulation(OFDM_Demodulationdata_out_2);
        data_out_2=P2SConverter(demodulationdata_outI_2,demodulationdata_outQ_2);
        data_out_2_tp = reshape(data_out_2, cod_num, cod_len);
        data_out_2_inter = reshape(data_out_2_tp.', 1, cod_len*cod_num);
        [be2_l,ber2]= symerr(cod_tx, data_out_2_inter);             %OFDM��һ��ֱ�ӵ���õ���������?
        be2 = be2 + be2_l;
        
        % 错误统计
        frm_num = frm_num +1;
        total_bit_num = frm_num * bit_len * cod_num;   % 参加统计的总比特数
%         m_iter = m_iter + iter_num- 1;      %   均衡迭代总次数
        
        total_error = sum(de_be + un_be);   % 总的错误比特数
        total_ferror = sum(de_fe + un_fe);  % 总的错误帧数
        ber = total_error/ total_bit_num;   % 可检测的误比特率
        fer = total_ferror / frm_num;       % 帧错误率
        ber2 = be2 / total_bit_num;
%         ave_n_iter = n_iter /  (frm_num * bnum); % 平均译码迭代次数
%         ave_m_iter = m_iter / frm_num ; %平均迭代次数
            
        % 打印仿真结果
        if (mod(frm_num,10)==0)
            fprintf('snr=%2.1f fnum=%d ber=%.8f ber2=%.8f fer=%.8f fe=%d\n',snr, frm_num, ber, ber2, fer, total_ferror);
        end

        % 判断是否退出仿真
        if total_ferror >= err_allow  %�?��条件
            gogo=0;
        end
        
        OFDM_LS_SNR_BER(snr_cnt) = ber;                                 %�洢OFDM������,LS
        snr_cnt=snr_cnt+1;
        
        
    end
end
%��ͼ
figure(1)  
SNR_1 = 0:1:30;%����ȴ�?��30dB
semilogy(SNR_1, OFDM_SNR_BER,'b-+');%
% hold on
% semilogy(SNR_1, OFDM_LS_SNR_BER,'r-*');%
% grid on
legend('OFDMֱ�ӽ��������?,')
xlabel('snr(dB)');
ylabel('BER');
title('jh');
