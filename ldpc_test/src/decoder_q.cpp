#include "float.h"
#include "sparse.h"
#include "check.h"
#include "decoder_q.h"
#include  "main.h"
#include <math.h>

int debug_cnt;

/***********************************************************************
以下为量化的译码算法
***********************************************************************/
 unsigned prprp_decode_q
( mod2sparse *H,	 
  int *llratio,	
  char *dblk,		
  char *pchk,		
  int *bprb,
  char *info,
  int *csum
)
{   int N, n,k,c=0,flag,num,j;
    extern int  Un_BER,De_BER,Un_FER,De_FER;
    extern char DecodedData[EncodedLength];
    extern int  Parity[ParityCheckNum][H_ColNum];

   N = mod2sparse_cols(H);
   
   initprp_quan(H,llratio,dblk,bprb);

   for (n = 0;n<max_iter; n++)
  {  c = check(H,dblk,pchk);
    if (c==0)
       break; 
	iterprp_quan_msn4v(H, llratio, dblk, bprb);
  } 

   c = check(H,dblk,pchk);
	*csum = c;

   flag=0;
   for (k = zero_len; k <InfoLength; k++)
   	   if( info[k] != DecodedData[k] )
	   {	if(c==0) {
		   Un_BER++;
		   num=0;
		   for(j=0;j<EncodedLength;j++)
			   if (DecodedData[j]!=info[k]) {
				   num++;
//				   fprintf(file,"%6d ",j);
				   
			   }
 //                fprintf(file,"%d\n",num);
	   
	   }
	        else
			De_BER++;
	        flag=1;
	   }

	if(flag==1)
	{
		if(c==0)
			Un_FER++;
		else
			De_FER++;
	}
  return n;
}


int satu(int x, int quan_max)
{
	int y;

	if (x > quan_max)
		y = quan_max;
	else
		if (x < -quan_max)
			y = -quan_max;
		else
			y = x;
	return y;
}

int quan(double x, double quan_step)
{
	int y,y_t;
	double y_dq;

	y_dq = x / quan_step;
	y_t = (int)y_dq;
	if (fabs(y_dq - y_t)>0.5)
		y = y_t + sgn(y_t);
	else
		y = y_t;
	y = satu(y, QMAX);
	return y;

}

double de_quan(int x, double quan_step)
{
	double y;

	y = x*quan_step;
	return y;
}

int tu(int x, int y, double quan_step, int quan_max)
{
	int z;
	double x_dq, y_dq, x_dq_l, y_dq_l, z_dq_l, z_dq;

//	if (x = -quan_max)
//		printf("xx");

	if (abs(x) > quan_max)
		x_dq = VMAX_VAL;
	else
		x_dq = de_quan(abs(x), quan_step);
	if (abs(y) > quan_max)
		y_dq = VMAX_VAL;
	else
		y_dq = de_quan(abs(y), quan_step);

	if (x_dq == 0)
		x_dq_l = VMAX_VAL;
	else if (x_dq >= VMAX_VAL)
		x_dq_l = 0;
	else
		x_dq_l = log((exp(x_dq) + 1)/(exp(x_dq) - 1));

	if (y_dq == 0)
		y_dq_l = VMAX_VAL;
	else if (y_dq >= VMAX_VAL)
		y_dq_l = 0;
	else
		y_dq_l = log((exp(y_dq) + 1)/(exp(y_dq) - 1));

	z_dq_l = x_dq_l + y_dq_l;

	if (z_dq_l == 0)
		z_dq = VMAX_VAL;
	else if (z_dq_l >= VMAX_VAL)
		z_dq = 0;
	else
		z_dq = log((exp(z_dq_l) + 1)/(exp(z_dq_l) - 1));
		
	z = quan(z_dq, quan_step);
	z = satu(z, quan_max);

	return z;
}

int tv(int x, int y, double quan_step, int quan_max)
{
	int z;
	double x_dq, y_dq, z_dq;

	x_dq = de_quan(x, quan_step);
	y_dq = de_quan(y, quan_step);

	z_dq = x_dq + y_dq;
		
	z = quan(z_dq, quan_step);
	z = satu(z, quan_max);

	return z;
}

 void initprp_quan
( mod2sparse *H,	
  int *llratio,	
  char   *dblk,	
  int *bprb
)
{ 
  mod2entry *e;
  int N;
  int j;
//  int quan_max;
//  double quan_step;
//  int a;
//  double b, bx;
  N = mod2sparse_cols(H);

//  quan_max = (int)pow(2, (bit_w-1))-1;
// quan_step = v_max/(quan_max+0.5);

  for (j = 0; j<N; j++)
  {  for (e = mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e = mod2sparse_next_in_col(e))
	  {	  e->prq = llratio[j];
          e->lrq = 0;
//		  a = quan(llratio[j], quan_step);
//		  b = de_quan(a, quan_step);
//		  bx = quan(2*quan_step, quan_step);
//		  bx = llratio[j];
//		  if ((fabs(bx - b) >= 0.5) || (sgn(a) != sgn(b)))
//			  printf("err: quantizing error!");
	  }
	  bprb[j] = llratio[j];
      dblk[j] = !(llratio[j]>=0);				//对数似然比定义为log(p0/p1)
	  //dblk[j] = !(lratio[j]>=1);				//似然比定义为p0/p1
   }
}


 /***********************************
 //  量化的对数似然比测度的和积算法
 //
 ***********************************/
void iterprp_quan
( mod2sparse *H,
  int *llratio,
  char *dblk,	
  int *bprb
)
{ int prq, dlq;  
  int slq; 
  
  mod2entry *e;
  int N, M;
  int i, j;
//  int tp1,tp2,tp3,tp4,tp5,tp6,tp7,tp8;
//	int quan_max;
//	double quan_step;


//	quan_max = (int)pow(2, (bit_w-1))-1;
//	quan_step = v_max/(quan_max+0.5);

	M = mod2sparse_rows(H);
	N = mod2sparse_cols(H);

for( i = 0;i < M;i++ )
  {   
	  dlq = QMAX;
	  slq = 1;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 
		  e->lrq = slq * dlq;

		  if (dlq>=abs(e->prq))
			  dlq = Tu[dlq*(dlq+1)/2+abs(e->prq)];			//e->prq add abs?
		  else
			  dlq = Tu[abs(e->prq)*(abs(e->prq)+1)/2+dlq];	
		  slq = slq * sgn(e->prq);
		  
	   }
      dlq = QMAX;
	  slq = 1;
      for (e = mod2sparse_last_in_row( H, i ); !mod2sparse_at_end( e );e = mod2sparse_prev_in_row(e) )
	  {
		  if (dlq>=abs(e->lrq))
			  e->lrq = slq * sgn(e->lrq) * Tu[dlq*(dlq+1)/2+abs(e->lrq)];			//e->prq add abs?
		  else
			  e->lrq = slq * sgn(e->lrq) * Tu[abs(e->lrq)*(abs(e->lrq)+1)/2+dlq];
		  if (dlq>=abs(e->prq))
			  dlq = Tu[dlq*(dlq+1)/2+abs(e->prq)];			//e->prq add abs?
		  else
			  dlq = Tu[abs(e->prq)*(abs(e->prq)+1)/2+dlq];
		  slq = slq * sgn(e->prq);
	  }
  }

  for( j = 0; j < N;j++ )
  {
	  prq = llratio[j];
      for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
	  { 
		e->prq = prq;
		prq = prq + e->lrq;
		if (prq>63) prq = 63;
		else if (prq<-63) prq = -63;
	  }
     bprb[j] = prq;					//在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
     dblk[j] = !(prq>=0);
     prq = 0;
     for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
    {	
		e->prq = e->prq + prq;
		if (e->prq>63) e->prq = 63;
		else if (e->prq<-63) e->prq = -63;
        prq = prq + e->lrq;
		if (prq>63) prq = 63;
		else if (prq<-63) prq = -63;
    }
  }

}


int Tu_cal(int x, int y)
{
	int Lmax, Lslow, z;

	if (x>=y)
	{	  
		if (x<=2)
			Lmax = 0;
		else if (x<=5)
			Lmax = 1;
		else if (x<=7)
			Lmax = x - 4;
		else if (x<=20)
			Lmax = x - 5;
		else
			Lmax = x - 6;

		Lslow = x - ((x-Lmax)<<1);

		if (Lslow<0)
			z = (y>>1);
		else if (y<=Lslow)
			z = y;
		else
			z = Lmax - ((x-y)>>1);
	}
	else
	{	  
		if (y<=2)
			Lmax = 0;
		else if (y<=5)
			Lmax = 1;
		else if (y<=7)
			Lmax = y - 4;
		else if (y<=20)
			Lmax = y - 5;
		else
			Lmax = y - 6;

		Lslow = y - ((y-Lmax)<<1);

		if (Lslow<0)
			z = x>>1;
		else if (x<=Lslow)
			z = x;
		else
			z = Lmax - ((y-x)>>1);
	}

	if (z<0)
		z = 0;
	else if (z > Lmax)
		z = Lmax;

	return z;
}

int Tu_caln(int x, int y)
{
	int Lmax, Lslow, z, Al, Ah;

	if (x>=y)
	{	  
		if (x<=2)
			Lmax = 0;
		else if (x<=5)
			Lmax = 1;
		else if (x<=7)
			Lmax = x - 4;
		else if (x<=20)
			Lmax = x - 5;
		else
			Lmax = x - 6;

		Lslow = x - ((x-Lmax)<<1);

		if (Lslow<0)
			z = (y>>1);
		else if (y<=Lslow)
			z = y;
		else
			z = Lmax - ((x-y)>>1);

		if (x>24)
			Al = x-20;
		else if (x>=22)
			Al = 4;
		else if (x>=20)
			Al = 3;
		else if (x>=16)
			Al = 2;
		else if (x>=11)
			Al = 1;
		else
			Al = QMAX;

		if (x>20)
			Ah = 6;
		else if (x>=11)
			Ah = 0;

		if (y>Al && y<x-Ah)
			z = z-1;
		
			
	}
	else
	{	  
		if (y<=2)
			Lmax = 0;
		else if (y<=5)
			Lmax = 1;
		else if (y<=7)
			Lmax = y - 4;
		else if (y<=20)
			Lmax = y - 5;
		else
			Lmax = y - 6;

		Lslow = y - ((y-Lmax)<<1);

		if (Lslow<0)
			z = x>>1;
		else if (x<=Lslow)
			z = x;
		else
			z = Lmax - ((y-x)>>1);

		if (y>24)
			Al = y-20;
		else if (y>=22)
			Al = 4;
		else if (y>=20)
			Al = 3;
		else if (y>=16)
			Al = 2;
		else if (y>=11)
			Al = 1;
		else
			Al = QMAX;

		if (y>20)
			Ah = 6;
		else if (y>=11)
			Ah = 0;

		if (x>Al && x<y-Ah)
			z = z-1;
	}


	if (z<0)
		z = 0;
	else if (z > Lmax)
		z = Lmax;

	return z;
}



int Tu_caln1(int x, int y)
{
	int Lmax, Lslow, z, Al, Ah;

	if (x>=y)
	{	  
		if (x<=2)
			Lmax = 0;
		else if (x<=5)
			Lmax = 1;
		else if (x<=7)
			Lmax = x - 4;
		else if (x<=20)
			Lmax = x - 5;
		else
			Lmax = x - 6;

		//Lslow = x - ((x-Lmax)<<1);
		if (x<=2)
			Lslow = -x;
		else if (x<=5)
			Lslow = 2 - x;
		else if (x<=7)
			Lslow = x - 8;
		else if (x<=20)
			Lslow = x - 10;
		else
			Lslow = x - 12;

		if (x>24)
			Al = x-20;
		else if (x>=22)
			Al = 4;
		else if (x>=20)
			Al = 3;
		else if (x>=16)
			Al = 2;
		else if (x>=11)
			Al = 1;
		else if (x>=10)
			Al = 0;
		else
			Al = QMAX;

		if (x>20)
			Ah = 6;
		else if (x>=11)
			Ah = 0;
		else if (x>=10)
			Ah = Lmax;

		if (y<=Al)
			z = y>>(int)(sgnx(Lslow));
		else if (y<=Lslow)
			z = y-1;
		else if (y<Lmax-Ah)
			z = Lmax - ((x-y)>>1) - 1;
		else
			z = Lmax - ((x-y)>>1);
		
			
	}
	else
	{	  
		if (y<=2)
			Lmax = 0;
		else if (y<=5)
			Lmax = 1;
		else if (y<=7)
			Lmax = y - 4;
		else if (y<=20)
			Lmax = y - 5;
		else
			Lmax = y - 6;

		Lslow = y - ((y-Lmax)<<1);

		if (y>24)
			Al = y-20;
		else if (y>=22)
			Al = 4;
		else if (y>=20)
			Al = 3;
		else if (y>=16)
			Al = 2;
		else if (y>=11)
			Al = 1;
		else if (y>=10)
			Al = 0;
		else
			Al = QMAX;

		if (y>20)
			Ah = 6;
		else if (y>=11)
			Ah = 0;
		else if (y>=10)
			Ah = Lmax;

		if (x<=Al)
			z = x>>(int)(sgnx(Lslow));
		else if (x<=Lslow)
			z = x-1;
		else if (x<Lmax-Ah)
			z = Lmax - ((y-x)>>1) - 1;
		else
			z = Lmax - ((y-x)>>1);
	}


	if (z<0)
		z = 0;
	else if (z > Lmax)
		z = Lmax;

	return z;
}


 /***************************************************************
 //  量化的对数似然比测度的和积算法，基于二维折线逼近的快速算法
 //
 ***************************************************************/
void iterprp_quan_n
( mod2sparse *H,
  int *llratio,
  char *dblk,	
  int *bprb
)
{ int prq, dlq;  
  int slq; 
  
  mod2entry *e;
  int N, M;
  int i, j;


	M = mod2sparse_rows(H);
	N = mod2sparse_cols(H);

for( i = 0;i < M;i++ )
  {   
	  dlq = QMAX;
	  slq = 1;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 
		  e->lrq = slq * dlq;
		  dlq = Tu_caln1(dlq, abs(e->prq));
		  slq = slq * sgn(e->prq);
		  
	   }
      dlq = QMAX;
	  slq = 1;
      for (e = mod2sparse_last_in_row( H, i ); !mod2sparse_at_end( e );e = mod2sparse_prev_in_row(e) )
	  {
		  e->lrq = slq * sgn(e->lrq) * Tu_caln1(dlq, abs(e->lrq));
		  dlq = Tu_caln1(dlq,abs(e->prq));
		  slq = slq * sgn(e->prq);
	  }
  }

  for( j = 0; j < N;j++ )
  {
	  prq = llratio[j];
      for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
	  { 
		e->prq = prq;
		prq = prq + e->lrq;
		if (prq>63) prq = 63;
		else if (prq<-63) prq = -63;
	  }
     bprb[j] = prq;					//在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
     dblk[j] = !(prq>=0);
     prq = 0;
     for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
    {	
		e->prq = e->prq + prq;
		if (e->prq>63) e->prq = 63;
		else if (e->prq<-63) e->prq = -63;
        prq = prq + e->lrq;
		if (prq>63) prq = 63;
		else if (prq<-63) prq = -63;
    }
  }

}

int min_sum_q(int x, int y)
{
	int z;
	int x_a, y_a;
	x_a = abs(x);
	y_a = abs(y);
	z = sgn(x) * sgn(y) * min(x_a, y_a);
	return z;
}

 /***************************************************************
 //  量化的对数似然比测度的最小和算法
 //
 ***************************************************************/
void iterprp_quan_ms
( mod2sparse *H,
  int *llratio,
  char *dblk,	
  int *bprb
)
{ int prq, dlq;  
  int slq; 
  
  mod2entry *e;
  int N, M;
  int i, j;


	M = mod2sparse_rows(H);
	N = mod2sparse_cols(H);

for( i = 0;i < M;i++ )
  {   
	  dlq = QMAX;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 
		  e->lrq = dlq;
		  dlq = min_sum_q(dlq, e->prq);
		  
	   }
      dlq = QMAX;
	  slq = 1;
      for (e = mod2sparse_last_in_row( H, i ); !mod2sparse_at_end( e );e = mod2sparse_prev_in_row(e) )
	  {
		  e->lrq = min_sum_q(dlq, e->lrq);
		  dlq = min_sum_q(dlq,e->prq);
	  }
  }

  for( j = 0; j < N;j++ )
  {
	  prq = llratio[j];
      for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
	  { 
		e->prq = prq;
		prq = prq + e->lrq;
		if (prq>63) prq = 63;
		else if (prq<-63) prq = -63;
	  }
     bprb[j] = prq;					//在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
     dblk[j] = !(prq>=0);
     prq = 0;
     for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
    {	
		e->prq = e->prq + prq;
		if (e->prq>63) e->prq = 63;
		else if (e->prq<-63) e->prq = -63;
        prq = prq + e->lrq;
		if (prq>63) prq = 63;
		else if (prq<-63) prq = -63;
    }
  }

}

/***************************************************************
 //  量化的对数似然比测度的修正最小和算法
 //
 ***************************************************************/
void iterprp_quan_msn
( mod2sparse *H,
  int *llratio,
  char *dblk,	
  int *bprb
)
{ int prq, dlq;  
  int slq; 
  
  mod2entry *e;
  int N, M;
  int i, j;
  int cnt;

  int dlq1, dlq2, dlq3;
  int pos1, pos2, pos3;


	M = mod2sparse_rows(H);
	N = mod2sparse_cols(H);

for( i = 0;i < M;i++ )
  {   
	  cnt = 0;
	  dlq1 = QMAX;
	  pos1 = 0;
	  slq = 1;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 
		  slq = slq * sgn(e->prq);
		  if (abs(e->prq) <= abs(dlq1))
		  {
			  pos1 = cnt;
			  dlq1 = e->prq;
		  }
		  cnt++;
	  }

	  cnt = 0;
	  dlq2 = QMAX;
	  pos2 = 0;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (abs(e->prq) <= abs(dlq2) && cnt!=pos1)
		  {
			  pos2 = cnt;
			  dlq2 = e->prq;
		  }
		  cnt++;
	  }

	  cnt = 0;
	  dlq3 = QMAX;
	  pos3 = 0;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (abs(e->prq) <= abs(dlq3) && cnt!=pos1 && cnt!=pos2)
		  {
			  pos3 = cnt;
			  dlq3 = e->prq;
		  }
		  cnt++;
	  }
	  cnt = 0;
	  for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (cnt!=pos1 && cnt!=pos2)
//		  if (cnt!=pos1)
			  e->lrq = slq*sgn(e->prq)*max((Tu_caln1(abs(dlq2), abs(dlq1))-0), 0);
		  else if (cnt==pos2)
			  e->lrq = slq*sgn(e->prq)*max((Tu_caln1(abs(dlq3), abs(dlq1))-0), 0);
		  else
			  e->lrq = slq*sgn(e->prq)*max((Tu_caln1(abs(dlq3), abs(dlq2))-0), 0);
		  cnt++;
	  }	  
  }

  for( j = 0; j < N;j++ )
  {
	  prq = llratio[j];
      for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
	  { 
		e->prq = prq;
		prq = prq + e->lrq;
		//if (prq>63) prq = 63;
		//else if (prq<-63) prq = -63;
	  }
	  if (prq>127) bprb[j] = 127;
	  else if (prq<-127) bprb[j] = -127;
	  else bprb[j] = prq;
     dblk[j] = !(prq>=0);    //在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
     prq = 0;
     for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
    {	
		e->prq = e->prq + prq;
		if (e->prq>63) e->prq = 63;
		else if (e->prq<-63) e->prq = -63;
        prq = prq + e->lrq;
		//if (prq>63) prq = 63;
		//else if (prq<-63) prq = -63;
    }
  }

}


/***************************************************************
 //  量化的对数似然比测度的修正最小和算法，文献"Modified min-sum 
 //			decoding algorithm for LDPC codes based on classified correction"
 //
 ***************************************************************/
void iterprp_quan_msn1
( mod2sparse *H,
  int *llratio,
  char *dblk,	
  int *bprb
)
{ int prq, dlq;  
  int slq; 
  
  mod2entry *e;
  int N, M;
  int i, j;
  int cnt;

  int dlq1, dlq2, dlq3;
  int pos1, pos2, pos3;
  int cf;


	M = mod2sparse_rows(H);
	N = mod2sparse_cols(H);

for( i = 0;i < M;i++ )
  {   
	  cnt = 0;
	  dlq1 = QMAX;
	  pos1 = 0;
	  slq = 1;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 
		  slq = slq * sgn(e->prq);
		  if (abs(e->prq) <= dlq1)
		  {
			  pos1 = cnt;
			  dlq1 = abs(e->prq);
		  }
		  cnt++;
	  }
	  cf =quan(log((double)cnt), QSTEP);

	  cnt = 0;
	  dlq2 = QMAX;
	  pos2 = 0;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (abs(e->prq) <= dlq2 && cnt!=pos1)
		  {
			  pos2 = cnt;
			  dlq2 = abs(e->prq);
		  }
		  cnt++;
	  }

	  if (dlq1>=(cf>>1))
		  dlq1 = dlq1 - (cf>>2);
	  else
		  dlq1 = dlq1;
	  if (dlq2>=(cf>>1)*3)
		  dlq2 = dlq2 - (cf>>1);
	  else
		  dlq2 = dlq2;
 

	  cnt = 0;
	  for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (cnt!=pos1)
			  e->lrq = slq*sgn(e->prq)*dlq1;
		  else
			  e->lrq = slq*sgn(e->prq)*dlq2;
		  cnt++;
	  }	  
  }

  for( j = 0; j < N;j++ )
  {
	  prq = llratio[j];
      for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
	  { 
		e->prq = prq;
		prq = prq + e->lrq;
		if (prq>63) prq = 63;
		else if (prq<-63) prq = -63;
	  }
     bprb[j] = prq;					//在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
     dblk[j] = !(prq>=0);
     prq = 0;
     for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
    {	
		e->prq = e->prq + prq;
		if (e->prq>63) e->prq = 63;
		else if (e->prq<-63) e->prq = -63;
        prq = prq + e->lrq;
		if (prq>63) prq = 63;
		else if (prq<-63) prq = -63;
    }
  }

}


int min_sum_oft(int x, int y)
{
	int z;
	int x_a, y_a, z_e;
	x_a = abs(x);
	y_a = abs(y);

	z_e = min(x_a, y_a) - quan(BETA, QSTEP);;
	if (z_e<0)
		z_e = 0;

	z = sgn(x) * sgn(y) * z_e;
	return z;
}

 /***************************************************************
 //  量化的对数似然比测度的最小和算法, offset修正
 //
 ***************************************************************/
void iterprp_quan_ms_oft
( mod2sparse *H,
  int *llratio,
  char *dblk,	
  int *bprb
)
{ int prq, dlq;  
  int slq; 
  
  mod2entry *e;
  int N, M;
  int i, j;



	M = mod2sparse_rows(H);
	N = mod2sparse_cols(H);

for( i = 0;i < M;i++ )
  {   
	  dlq = QMAX;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 
		  e->lrq = dlq;
		  dlq = min_sum_oft(dlq, e->prq);
		  
	   }
      dlq = QMAX;
	  slq = 1;
      for (e = mod2sparse_last_in_row( H, i ); !mod2sparse_at_end( e );e = mod2sparse_prev_in_row(e) )
	  {
		  e->lrq = min_sum_oft(dlq, e->lrq);
		  dlq = min_sum_oft(dlq,e->prq);
	  }
  }

  for( j = 0; j < N;j++ )
  {
	  prq = llratio[j];
      for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
	  { 
		e->prq = prq;
		prq = prq + e->lrq;
		if (prq>63) prq = 63;
		else if (prq<-63) prq = -63;
	  }
     bprb[j] = prq;					//在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
     dblk[j] = !(prq>=0);
     prq = 0;
     for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
    {	
		e->prq = e->prq + prq;
		if (e->prq>63) e->prq = 63;
		else if (e->prq<-63) e->prq = -63;
        prq = prq + e->lrq;
		if (prq>63) prq = 63;
		else if (prq<-63) prq = -63;
    }
  }

}


/***************************************************************
 //  对变量信息进行加权平均的量化的对数似然比测度的修正最小和算法
 //
 ***************************************************************/
void iterprp_quan_msn_iir
( mod2sparse *H,
  int *llratio,
  char *dblk,	
  int *bprb
)
{ int prq, dlq;  
  int slq; 
  
  mod2entry *e;
  int N, M;
  int i, j;
  int cnt;
  int k, prq_old[8];
  int prq_t;

  int dlq1, dlq2, dlq3;
  int pos1, pos2, pos3;


	M = mod2sparse_rows(H);
	N = mod2sparse_cols(H);

for( i = 0;i < M;i++ )
  {   
	  cnt = 0;
	  dlq1 = QMAX;
	  pos1 = 0;
	  slq = 1;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 
		  slq = slq * sgn(e->prq);
		  if (abs(e->prq) <= abs(dlq1))
		  {
			  pos1 = cnt;
			  dlq1 = e->prq;
		  }
		  cnt++;
	  }

	  cnt = 0;
	  dlq2 = QMAX;
	  pos2 = 0;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (abs(e->prq) <= abs(dlq2) && cnt!=pos1)
		  {
			  pos2 = cnt;
			  dlq2 = e->prq;
		  }
		  cnt++;
	  }

	  cnt = 0;
	  dlq3 = QMAX;
	  pos3 = 0;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (abs(e->prq) <= abs(dlq3) && cnt!=pos1 && cnt!=pos2)
		  {
			  pos3 = cnt;
			  dlq3 = e->prq;
		  }
		  cnt++;
	  }
	  cnt = 0;
	  for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (cnt!=pos1 && cnt!=pos2)
//		  if (cnt!=pos1)
			  e->lrq = slq*sgn(e->prq)*max((Tu_caln1(abs(dlq2), abs(dlq1))-1), 0);
		  else if (cnt==pos2)
			  e->lrq = slq*sgn(e->prq)*max((Tu_caln1(abs(dlq2), abs(dlq1))-1), 0);
		  else
			  e->lrq = slq*sgn(e->prq)*max((Tu_caln1(abs(dlq3), abs(dlq2))-1), 0);
		  cnt++;
	  }	  
  }

	for( j = 0; j < N;j++ )
	{
		prq = llratio[j];
		for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
		{ 
			e->prq = prq;
			prq = prq + e->lrq;
			//if (prq>63) prq = 63;
			//else if (prq<-63) prq = -63;
		}
		if (prq>255) bprb[j] = 255;
		else if (prq<-255) bprb[j] = -255;
		else bprb[j] = prq;
		dblk[j] = !(prq>=0);    //在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
		prq = 0;
		for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
		{	
			e->prq = e->prq + prq;
			if (e->prq>63) e->prq = 63;
			else if (e->prq<-63) e->prq = -63;
			prq = prq + e->lrq;
			//if (prq>63) prq = 63;
			//else if (prq<-63) prq = -63;
		}
	}

}


/***************************************************************
 //  量化的对数似然比测度的修正最小和算法，增加对12bit已知信息的利用
 //
 ***************************************************************/
void iterprp_quan_msn_12b
( mod2sparse *H,
  int *llratio,
  char *dblk,	
  int *bprb
)
{ int prq, dlq;  
  int slq; 
  
  mod2entry *e;
  int N, M;
  int i, j;
  int cnt;

  int dlq1, dlq2, dlq3;
  int pos1, pos2, pos3;


	M = mod2sparse_rows(H);
	N = mod2sparse_cols(H);

for( i = 0;i < M;i++ )
  {   
	  cnt = 0;
	  dlq1 = QMAX+1;
	  pos1 = 0;
	  slq = 1;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 
		  slq = slq * sgn(e->prq);
		  if (abs(e->prq) < abs(dlq1))
		  {
			  pos1 = cnt;
			  dlq1 = e->prq;
		  }
		  cnt++;
	  }

	  cnt = 0;
	  dlq2 = QMAX+1;
	  pos2 = 0;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (abs(e->prq) < abs(dlq2) && cnt!=pos1)
		  {
			  pos2 = cnt;
			  dlq2 = e->prq;
		  }
		  cnt++;
	  }

	  cnt = 0;
	  dlq3 = QMAX+1;
	  pos3 = 0;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (abs(e->prq) < abs(dlq3) && cnt!=pos1 && cnt!=pos2)
		  {
			  pos3 = cnt;
			  dlq3 = e->prq;
		  }
		  cnt++;
	  }
	  cnt = 0;
	  for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	 

		  if (cnt!=pos1 && cnt!=pos2)
//		  if (cnt!=pos1)
			  e->lrq = slq*sgn(e->prq)*max((Tu_caln1(abs(dlq2), abs(dlq1))-1), 0);
		  else if (cnt==pos2)
			  e->lrq = slq*sgn(e->prq)*max((Tu_caln1(abs(dlq2), abs(dlq1))-1), 0);
		  else
			  e->lrq = slq*sgn(e->prq)*max((Tu_caln1(abs(dlq3), abs(dlq2))-1), 0);
		  cnt++;
	  }	  
  }

	for( j = 0; j < 12;j++ )
	{
		for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
		{ 
			e->prq = 63;
		}
		bprb[j] = 255;
		dblk[j] = 0;    //在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
	}

	for( j = 12; j < N;j++ )
	{
		prq = llratio[j];
		for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
		{ 
			e->prq = prq;
			prq = prq + e->lrq;
			//if (prq>63) prq = 63;
			//else if (prq<-63) prq = -63;
		}
		if (prq>255) bprb[j] = 255;
		else if (prq<-255) bprb[j] = -255;
		else bprb[j] = prq;
		dblk[j] = !(prq>=0);    //在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
		prq = 0;
		for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
		{	
			e->prq = e->prq + prq;
			if (e->prq>63) e->prq = 63;
			else if (e->prq<-63) e->prq = -63;
			prq = prq + e->lrq;
			//if (prq>63) prq = 63;
			//else if (prq<-63) prq = -63;
		}
	}

}


/***************************************************************
//  量化的对数似然比测度的修正最小和算法,对比调试FPGA代码用
//
***************************************************************/
void iterprp_quan_msn4v
(mod2sparse *H,
int *llratio,
char *dblk,
int *bprb
)
{
	int prq, dlq;
	int slq;

	mod2entry *e;
	int N, M;
	int i, j;
	int cnt;

	int dlq1, dlq2, dlq3;
	int pos1, pos2, pos3;


	M = mod2sparse_rows(H);
	N = mod2sparse_cols(H);

	for (i = 0; i < M; i++)
	{
		// (i == 6144)
			//bug_cnt++;

		cnt = 0;
		dlq1 = QMAX;
		pos1 = 0;
		slq = 1;
		for (e = mod2sparse_first_in_row(H, i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e))
		{
			slq = slq * sgn(e->prq);
			if (abs(e->prq) <= abs(dlq1))
			{
				pos1 = cnt;
				dlq1 = e->prq;
			}
			cnt++;
		}

		cnt = 0;
		dlq2 = QMAX;
		pos2 = 0;
		for (e = mod2sparse_first_in_row(H, i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e))
		{

			if (abs(e->prq) <= abs(dlq2) && cnt != pos1)
			{
				pos2 = cnt;
				dlq2 = e->prq;
			}
			cnt++;
		}

		cnt = 0;
		dlq3 = QMAX;
		pos3 = 0;
		for (e = mod2sparse_first_in_row(H, i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e))
		{

			if (abs(e->prq) <= abs(dlq3) && cnt != pos1 && cnt != pos2)
			{
				pos3 = cnt;
				dlq3 = e->prq;
			}
			cnt++;
		}
		cnt = 0;
		for (e = mod2sparse_first_in_row(H, i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e))
		{

			if (cnt != pos1 && cnt != pos2)
			{
				if (abs(dlq2) == abs(dlq1))
					e->lrq = slq*sgn(e->prq)*(Tu_caln1(abs(dlq2), max(abs(dlq1) - 1, 0)));
				else
					e->lrq = slq*sgn(e->prq)*(Tu_caln1(max(abs(dlq2) - 1, 0), abs(dlq1)));
			}
			else if (cnt == pos2)
			{
				if (abs(dlq3) == abs(dlq1))
					e->lrq = slq*sgn(e->prq)*(Tu_caln1(abs(dlq3), max(abs(dlq1) - 1, 0)));
				else
					e->lrq = slq*sgn(e->prq)*(Tu_caln1(max(abs(dlq3) - 1, 0), abs(dlq1)));
			}
			else
			{
				if (abs(dlq3) == abs(dlq2))
					e->lrq = slq*sgn(e->prq)*(Tu_caln1(abs(dlq3), max(abs(dlq2) - 1, 0)));
				else
					e->lrq = slq*sgn(e->prq)*(Tu_caln1(max(abs(dlq3) - 1, 0), abs(dlq2)));
			}
			cnt++;
		}
	}

	for (j = 0; j < N; j++)
	{
		if (j == 83)
			debug_cnt++;
		prq = llratio[j];
		for (e = mod2sparse_first_in_col(H, j); !mod2sparse_at_end(e); e = mod2sparse_next_in_col(e))
		{
			e->prq = prq;
			prq = prq + e->lrq;
			//if (prq>63) prq = 63;
			//else if (prq<-63) prq = -63;
		}
		if (prq>127) bprb[j] = 127;
		else if (prq<-127) bprb[j] = -127;
		else bprb[j] = prq;
		dblk[j] = !(prq >= 0);    //在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
		prq = 0;
		for (e = mod2sparse_last_in_col(H, j); !mod2sparse_at_end(e); e = mod2sparse_prev_in_col(e))
		{
			e->prq = e->prq + prq;
			if (e->prq>63) e->prq = 63;
			else if (e->prq<-63) e->prq = -63;
			prq = prq + e->lrq;
			//if (prq>63) prq = 63;
			//else if (prq<-63) prq = -63;
		}
	}

}


