typedef struct complex_type complex_t;

struct complex_type
{
	double real;
	double imag;
};

double gauss(double n0);
void awgn(char *transmit, double *receive, double n0);
void code_ch(char *transmit, double *receive);
void ComplexGauss(double n0, complex_t *cg);
void rayleigh(double n0, complex_t *codeword, complex_t *receive);