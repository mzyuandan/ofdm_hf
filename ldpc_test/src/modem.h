void bpsk(char *data, char *mod_data);
void de_bpsk(double *data, double *llratio, double n0);
void qpsk(char *data, double *mod_data);
void de_qpsk(double *receive, double *LLR, double N0);
void Eight_psk(char *data, double *mod_data);
void de_8psk(double *receive, double *LLR, double N0);