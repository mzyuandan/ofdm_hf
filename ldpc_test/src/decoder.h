/* PROCEDURES RELATING TO DECODING METHODS. */
void prprp_decode_setup (void);
unsigned prprp_decode ( mod2sparse *H, double *llratio, char *dblk, char *pchk, double *bprb, char *info, int *csum);
void initprp ( mod2sparse *H, double *llratio,	char   *dblk, double *bprb );
void iterprp (mod2sparse *, double *, char *, double *);
void iterprp_ms (mod2sparse *, double *, char *, double *);
void iterprp_lr (mod2sparse *, double *, char *, double *);